﻿using System.Web;
using System.Web.Optimization;

namespace Critical.Fit
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
					bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
									"~/assets/js/jquery.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/mainjs").Include(
                        "~/assets/js/jquery.scrollex.min.js",
                        "~/assets/js/jquery.scrolly.min.js",
                        "~/assets/js/skel.min.js",
												"~/assets/js/jquery.validate.min.js",
												"~/assets/js/jquery.validate.unobtrusive.min.js",
												"~/assets/js/respond.min.js",
                        "~/assets/js/util.js",
                        "~/assets/js/main.js"));

         
            bundles.Add(new StyleBundle("~/css").Include(
                      "~/assets/css/main.css"));
        }
    }
}
