﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Critical.Fit.Models
{
	public class ContactUsModel
	{
		public ContactUsModel() { }
		[MaxLength(50)]
		[Required(ErrorMessage = "Name is required")]
		public string Name { get; set; }

		[DataType(DataType.EmailAddress, ErrorMessage = "Must provide valid email address.")]
		[Required(ErrorMessage = "Email Address is required.")]
		[MaxLength(50)]
		[EmailAddress]
		public string Email { get; set; }
		
		[MaxLength(1000)]
		[Required(ErrorMessage = "Message is required")]
		public string Message { get; set; }
	}
}