﻿using Critical.Fit.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace Critical.Fit.Controllers
{
	public class HomeController : Controller
	{
		public ActionResult Index()
		{
			return View();
		}

		[ReCaptcha]
		[HttpPost]
		public ActionResult ContactUs(ContactUsModel model)
		{
			if (ModelState.IsValid)
			{
				try
				{
					var email_to = System.Web.Configuration.WebConfigurationManager.AppSettings["criticalFitToEmail"];
					var email_from = System.Web.Configuration.WebConfigurationManager.AppSettings["criticalFitFromEmail"];
					SmtpClient smtp_client = new SmtpClient();

					MailMessage mail = new MailMessage();

					//Setting From , To and CC
					mail.From = new MailAddress(email_from);
					mail.To.Add(new MailAddress(email_to));
					mail.Subject = "Contact us from Critical.Fit site";
					mail.Body = String.Format(@"From: {0} <br />Email: {1} <br />Message: {2}", model.Name, model.Email, model.Message);
					mail.IsBodyHtml = true;

					smtp_client.Send(mail);
				}
				catch (Exception e)
				{
					return Json(new { is_valid = false, message = e.InnerException.Message });
				}
			}
			return Json(new { is_valid = ModelState.IsValid, message = ModelState.IsValid.ToString() });
		}
	}
}