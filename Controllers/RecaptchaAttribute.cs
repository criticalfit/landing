﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Critical.Fit.Controllers
{
	public enum Theme { Red, White, BlackGlass, Clean }
	public class MyObject
	{
		public string success { get; set; }
	}
	[Serializable]
	public class InvalidKeyException : ApplicationException
	{
		public InvalidKeyException() { }
		public InvalidKeyException(string message) : base(message) { }
		public InvalidKeyException(string message, Exception inner) : base(message, inner) { }
	}

	public class ReCaptchaAttribute : System.Web.Mvc.ActionFilterAttribute
	{
		public override void OnActionExecuting(ActionExecutingContext filterContext)
		{
			var privateKey = System.Web.Configuration.WebConfigurationManager.AppSettings["reCaptchaKey"];

			if (string.IsNullOrWhiteSpace(privateKey))
				throw new InvalidKeyException("ReCaptcha.PrivateKey missing from appSettings");

			var postData = string.Format("&secret={0}&response={1}",
																	 privateKey,
																	 filterContext.RequestContext.HttpContext.Request.Form["g-recaptcha-response"]);

			var postDataAsBytes = Encoding.UTF8.GetBytes(postData);

			// Create web request
			var request = WebRequest.Create("https://www.google.com/recaptcha/api/siteverify");
			request.Method = "POST";
			request.ContentType = "application/x-www-form-urlencoded";
			request.ContentLength = postDataAsBytes.Length;
			var dataStream = request.GetRequestStream();
			dataStream.Write(postDataAsBytes, 0, postDataAsBytes.Length);
			dataStream.Close();

			// Get the response.
			var response = request.GetResponse();

			using (dataStream = response.GetResponseStream())
			{
				using (var reader = new StreamReader(dataStream))
				{
					var responseFromServer = reader.ReadToEnd();
					JavaScriptSerializer js = new JavaScriptSerializer();
					MyObject data = js.Deserialize<MyObject>(responseFromServer);// Deserialize Json

					if (!Convert.ToBoolean(data.success))
						((System.Web.Mvc.Controller)filterContext.Controller).ModelState.AddModelError("ReCaptcha", "Captcha is incorrect");
				}
			}
		}
	}

}